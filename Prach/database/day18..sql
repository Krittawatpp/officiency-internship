create table department(
    name varchar(200),
    budget int not null,
    primary key (name)
);

create table employee(
    id int, 
    department_name varchar(200) not null,
    name varchar(200) not null,
    address varchar(200) not null,
    salary int not null,
    primary key (id),
    foreign key (department_name)
        references department (name)
);

create table supplier(
    id int,
    name varchar(200) not null,
    address varchar(500) not null,
    phone varchar(20) not null,
    primary key (id)
);

create table products(
    id int,
    supplier_id int not null,
    name varchar(200) not null,
    description varchar(1000) not null,
    price int not null,
    quantity int not null,
    primary key (id),
    foreign key (supplier_id)
        references supplier (id)
);

create table customer(
    id int primary key,
    name varchar(30) not null,
    address varchar(200) not null    
);

create table orders(
    id int, 
    date timestamp,
    employee_id int not null references employee(id),
    customer_id int not null references customer(id)
   
);

 create table order_item(
  
    id int, 
    amount int,
    discount int,
    primary key (id, product_id),
    order_item_id int not null references orders(id),
    product_id int not null references product(id)
    
);